You can use the [multvardiv](https://forgemia.inra.fr/imhorphen/multvardiv) package,
which provides the same tools for the multivariate $t$ distribution
and other multvariate distributions.

# mstudentd

This package provides tools for multivariate $t$ distributions (MTD):

* Calculation of distance/divergence between multivariate $t$ distributions (MTD):
  + Renyi divergence
  + Kullback divergence
* Manipulation of MTD distributions:
  + Probability density
  <!-- + Estimation of the parameters -->
  + Simulation of samples from a MTD
  + Plot of the density of a MTD with 2 variables

## Installation

Install the package from CRAN:
```
install.packages("mstudentd")
```


Or install the development version from the repository, using the `devtools` package:

```
install.packages("devtools")
devtools::install_git("https://forgemia.inra.fr/imhorphen/mstudentd")
```

## Authors

[Pierre Santagostini](mailto:pierre.santagostini@agrocampus-ouest.fr) and [Nizar Bouhlel](mailto:nizar.bouhlel@agrocampus-ouest.fr)

## Reference

N. Bouhlel and D. Rousseau (2023), Exact Rényi and Kullback-Leibler Divergences Between Multivariate t-Distributions, IEEE Signal Processing Letters.
<https://doi.org/10.1109/LSP.2023.3324594>
